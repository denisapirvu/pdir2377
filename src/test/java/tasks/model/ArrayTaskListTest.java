package tasks.model;

import org.junit.jupiter.api.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ArrayTaskListTest {


    private Task task;
    private String title;
    private int interval;
    private Date dateStart,dateEnd;
    private SimpleDateFormat sdf;

    @BeforeEach
    void setUp() {
        sdf = Task.getDateFormat();
        try
        {
            dateStart = sdf.parse("2020-14-23 08:00");
            dateEnd = sdf.parse("2020-03-23 12:00");


        }catch (ParseException e)
        {
            fail(e.getMessage());
        }
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    @DisplayName("TestECP")
    void ECP_non_valid1() {
        //set up
        TaskList taskList = new ArrayTaskList();

        //act
        try {
            taskList.add(new Task("", dateStart,dateEnd,2));
        }catch(IllegalArgumentException e){
            assertEquals(e.getMessage(),"Title cannot be empty");
        }
        //assert
    }
    //@Test
    @RepeatedTest(2)
    void ECP_valid(){
        TaskList taskList = new ArrayTaskList();
        taskList.add(new Task("ads", dateStart,dateEnd,2));
        assertEquals(taskList.size(),1);
    }
    @Test
    @Order(2)
    void ECP_non_valid2()
    {
        TaskList taskList = new ArrayTaskList();
        try {
            taskList.add(new Task("add",dateStart,dateEnd,-2));

        }catch(IllegalArgumentException e){
            assertEquals(e.getMessage(),"interval should be > 1");
        }

    }
    @Test
    @Order(1)
    void BVA_valid_1(){
        TaskList taskList = new ArrayTaskList();
        taskList.add(new Task("a", dateStart,dateEnd,2));
        assertEquals(taskList.size(),1);
    }
    @Test
    void BVA_valid_2(){
        TaskList taskList = new ArrayTaskList();
        taskList.add(new Task("ads", dateStart,dateEnd,1));
        assertEquals(taskList.size(),1);
    }
    @Test
    void BVA_non_valid_1(){
        TaskList taskList = new ArrayTaskList();
        try {
            taskList.add(new Task("SGJBYbTIU1ZqKuIRCIR669XljuFye0PceJxKOkwz9tINSetLDPRQqsB5mwWGq9fpjpMQYfAFy3jo2MZ1mNEN5vZyoaXF29EzxKelYLj4uUevqisLyuI1e89wZAyEkCSOVkH9iPusRqbmuCA6VPLwSitzJCa8NYXEWB4Y049fziPzAnyHnkekItaEnQaRC2bjPb736aPg5hNXTJmqeAlnJ718ZaBGrXasBo7bYVRPs4n1Zed8xZfSiGYZwm4tKlP9", dateStart,dateEnd));
        }catch(IllegalArgumentException e){
           // System.out.println(e.getMessage());
            assertEquals(e.getMessage(),"Title cannot be that long");
        }
    }
    @Test
    void BVA_non_valid_2(){
        TaskList taskList = new ArrayTaskList();
        try {
            taskList.add(new Task("add",dateStart,dateEnd,0));

        }catch(IllegalArgumentException e){
            assertEquals(e.getMessage(),"interval should be > 1");
        }
    }
    @Disabled
    @Test
    void test()
    {
        System.out.println("dfk");
    }

}