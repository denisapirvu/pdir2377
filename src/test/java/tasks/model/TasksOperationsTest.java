package tasks.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TasksOperationsTest {
    private Task task;
    private ObservableList<Task> tasksList;
    private TasksOperations tasksOperations;
    public ArrayList<Task> tasks;
    private String title;
    private int interval;
    private SimpleDateFormat sdf;

    @BeforeEach
    void setUp() {
        sdf=Task.getDateFormat();
        tasks = new ArrayList<>();
        try {
            Task task1 = new Task("task1",sdf.parse("2020-03-23 08:00"),sdf.parse("2020-03-25 12:00"),2);
            Task task2 = new Task("task2",sdf.parse("2020-03-26 12:00"),sdf.parse("2020-03-27 12:00"),2);
            Task task3 = new Task("task3",sdf.parse("2020-04-20 12:00"),sdf.parse("2020-04-28 12:00"),2);
            Task task4 = new Task("task4",sdf.parse("2020-03-19 12:00"),sdf.parse("2020-03-23 12:00"),2);
            Task task5 = new Task("task5",sdf.parse("2020-04-18 12:00"),sdf.parse("2020-04-23 12:00"),2);
            Task task6 = new Task("task6",sdf.parse("2020-05-23 12:00"),sdf.parse("2020-05-27 12:00"),2);

            task1.setActive(true);
            task2.setActive(true);
            task3.setActive(true);
            task4.setActive(true);
            task5.setActive(true);

            tasks.add(task1);
            tasks.add(task2);
            tasks.add(task3);
            tasks.add(task4);
            tasks.add(task5);
            tasks.add(task6);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        tasksList = FXCollections.observableList((ArrayList)tasks);
        tasksOperations = new TasksOperations(tasksList);

    }

    @AfterEach
    void tearDown() {
    }
    @Test
    void testEndBeforeStart(){
        try {
            Date dataStart =sdf.parse("2020-04-20 12:00");
            Date dataEnd =sdf.parse("2020-04-18 12:00");
            tasksOperations.incoming(dataStart,dataEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch(IllegalArgumentException ee){
            assertEquals(ee.getMessage(),"end date can't be before the start date");
           // System.out.println(ee.getMessage());
        }

    }

    @Test
    void testOK(){
        Iterable<Task> incomingTasks = new ArrayList<>();
        List<Task> list = new ArrayList<Task>();
        try {
            Date dataStart =sdf.parse("2020-04-17 12:00");
            Date dataEnd =sdf.parse("2020-04-24 12:00");
            incomingTasks = tasksOperations.incoming(dataStart,dataEnd);
            incomingTasks.forEach(list::add);
            assertEquals(list.size(),2);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch(IllegalArgumentException ee){
            ee.printStackTrace();
        }

    }
    @Test
    void testOK2(){
        Iterable<Task> incomingTasks = new ArrayList<>();
        List<Task> list = new ArrayList<Task>();
        try {
            Date dataStart =sdf.parse("2020-04-26 12:00");
            Date dataEnd =sdf.parse("2020-04-27 19:00");
            incomingTasks = tasksOperations.incoming(dataStart,dataEnd);
            incomingTasks.forEach(list::add);
            assertEquals(list.size(),1);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch(IllegalArgumentException ee){
            ee.printStackTrace();
        }

    }
    @Test
    void testOK3(){
        Iterable<Task> incomingTasks = new ArrayList<>();
        List<Task> list = new ArrayList<Task>();
        try {
            Date dataStart =sdf.parse("2020-03-20 12:00");
            Date dataEnd =sdf.parse("2020-04-29 12:00");
            incomingTasks = tasksOperations.incoming(dataStart,dataEnd);
            incomingTasks.forEach(list::add);
            assertEquals(list.size(),5);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch(IllegalArgumentException ee){
            ee.printStackTrace();
        }

    }
    @Test
    void testNextTimeNull(){
        Iterable<Task> incomingTasks = new ArrayList<>();
        List<Task> list = new ArrayList<Task>();
        try {
            Date dataStart =sdf.parse("2020-05-21 12:00");
            Date dataEnd =sdf.parse("2020-06-18 12:00");
            incomingTasks = tasksOperations.incoming(dataStart,dataEnd);
            incomingTasks.forEach(list::add);
            assertEquals(list.size(),0);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch(IllegalArgumentException ee){
            ee.printStackTrace();
        }

    }
    @Test
    void testTimeTask(){
        Iterable<Task> incomingTasks = new ArrayList<>();
        List<Task> list = new ArrayList<Task>();
        try {
            Date dataStart =sdf.parse("2020-07-20 12:00");
            Date dataEnd =sdf.parse("2020-07-29 12:00");
            incomingTasks = tasksOperations.incoming(dataStart,dataEnd);
            incomingTasks.forEach(list::add);
            assertEquals(list.size(),0);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch(IllegalArgumentException ee){
            ee.printStackTrace();
        }

    }

}